﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEIoT.DemandSheddingAlert.WebApi.Models;
using WEIoT.DemandSheddingAlert.WebApi.Services;

namespace WEIoT.DemandSheddingAlert.WebApi
{
    public interface IAlertRule
    {

        public Task<AlertResponse> ProcessAlert(AlertRequest alertRequest);

    }



    //public class AlertRule : IAlertRule
    //{

    //    private IXmattersService _xMattersService;
    //    private IEmailService _emailService;

    //    public AlertRule(IXmattersService xmalert, IEmailService emailalert)
    //    {
    //        _xMattersService = xmalert;
    //        _emailService = emailalert;
    //    }


    //    public async Task<AlertResponse> ProcessAlert(AlertRequest alertRequesta)
    //    {
    //        AlertResponse response = new AlertResponse();

    //        // Business logic needs to be added and process alert accordingly

    //        response.IsXmattersSuccess = await _xMattersService.TriggerAlert(new Alert());
    //        return response;

    //    }
    //}

}
