﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WEIoT.DemandSheddingAlert.WebApi.Models;

namespace WEIoT.DemandSheddingAlert.WebApi.Services
{

    public interface IXmattersService
    {

        public Task<bool> TriggerAlert(Alert alert);

    }


    public class XmattersService : IXmattersService
    {
        public async Task<bool> TriggerAlert(Alert alert)
        {
            //throw new NotImplementedException();

            /*

        1. Receive Alert object.
        2. Serialize it to Json
        3. use HTTPClient and trigger alert

         */
            string jsonData = "";

            using (HttpClient client = new HttpClient())
            {
                using (var content = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json"))
                {
                    using (var response = await client.PostAsync(new Uri("Https://www.google.com"), content).ConfigureAwait(false))
                    {
                        return (response.StatusCode == System.Net.HttpStatusCode.Accepted) ? true : false;
                    }
                }

            }

        }
    }
}
