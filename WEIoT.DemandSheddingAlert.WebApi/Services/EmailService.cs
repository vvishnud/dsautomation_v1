﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEIoT.DemandSheddingAlert.WebApi.Models;

namespace WEIoT.DemandSheddingAlert.WebApi.Services
{



    public interface IEmailService
    {

        public bool TriggerAlert(Alert alert);

    }



    public class EmailService : IEmailService
    {
        /*
        
        1. Receive Alert object.
        2. Use to Alert bject to Build SMTP object
        3. Trigger email alert
         
         */
        public  bool TriggerAlert(Alert alert)
        {
            return true;
        }
    }
}
