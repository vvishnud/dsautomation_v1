﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WEIoT.DemandSheddingAlert.WebApi.Models;

namespace WEIoT.DemandSheddingAlert.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DemandSheddingAlertController : ControllerBase
    {

        private IAlertRule _alert;
        public DemandSheddingAlertController(IAlertRule alert)
        {
            _alert = alert;
        }


        //[HttpGet]
        //public IActionResult Someget()
        //{
        //    return Ok("I am Alive");
        //}

        [HttpPost]
        public async Task<IActionResult> SendAlert([FromBody] AlertRequest alertRequest)
        {
            /*
             * 
             * 
             * */

            AlertResponse response= await _alert.ProcessAlert(alertRequest);
            return Ok();

        }





    }
}