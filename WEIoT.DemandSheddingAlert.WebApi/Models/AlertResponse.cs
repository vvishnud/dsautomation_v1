﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEIoT.DemandSheddingAlert.WebApi.Models
{
    public class AlertResponse
    {

        public bool IsXmattersSuccess { get; set; }
        public bool IsEmailSuccess { get; set; }
    }
}
