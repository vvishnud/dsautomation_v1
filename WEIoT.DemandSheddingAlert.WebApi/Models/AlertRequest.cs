﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEIoT.DemandSheddingAlert.WebApi.Models
{


    /// <summary>
    /// This class is a Model for the source calling the API via Http Post 
    /// </summary>

    public class AlertRequest
    {

        public string ScheduledEventId { get; set; }
        public string StoreNumber { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime TimeStamp_UTC { get; set; }

        public string AssetNumber { get; set; }

        public string EventPhase { get; set; }
    }

    #region commented
    //public class AssetSchema:StoreSchema
    //{

    //    public string AssetNumber { get; set; }

    //    public string EventPhase { get; set; }
    //}
    #endregion

    /// <summary>
    /// This class is an extension of Alert Request Model to suite xMatters Json schema Structure.
    /// </summary>

    public class Alert : AlertRequest
    {

        public string AlertDescription { get; set; }
        public string Recipients { get; set; }

        public string Priority { get; set; }
    }



}
